"""Methods for preprocessing image data"""

def _valspec(_):
    """Hook to do extra validation"""
    pass
PreprocessConfigSpec = {
    'GaussianBlur':
    Config.Param(default=0,
                 comment="Amount to blur"),
    'Downsample':
    Config.Param(default=0,
                 comment="Amount to downsample (must be integer power of 2)"),
    '_resource': "Preprocess",
    '_validation_hook': _valspec}


def Preprocess(cf, I):
    """Preprocess an image"""

    if cf.GaussianBlur > 0:
        # TODO: blur the image with this sigma
    
    if cf.Downsample > 0:
        # TODO: downsample by this factor
